package com.base;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class DataBaseConnection extends TestBase {

    public static Connection connection;

    public static String getPropertyFile(String filepath, String key) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(filepath));
        return properties.getProperty(key);
    }

    public static void connectToSQL() throws IOException, SQLException {
        String url = "jdbc:mysql://localhost:3306/worldOfAutomation";
        String username = getPropertyFile("src/main/resources/dbcredential.properties", "username");
        String password = getPropertyFile("src/main/resources/dbcredential.properties", "password");
        connection = DriverManager.getConnection(url, username, password);
    }
}

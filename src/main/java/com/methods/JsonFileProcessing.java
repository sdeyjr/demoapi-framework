package com.methods;

import com.classobj.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.methods.DataBaseMethods.getMapFromDBOfSingleRecord;

public class JsonFileProcessing {

    @SneakyThrows
    public static JSONArray getJsonArray(String path) {
        JSONParser jsonParser = new JSONParser();
        JSONArray jsonArray = (JSONArray) jsonParser.parse(new FileReader(path));
        return jsonArray;
    }

    public void getRecordsFromDB() throws SQLException, IOException {
       Map<String, String> data = getMapFromDBOfSingleRecord("SELECT * FROM worldOfAutomation.employee");

        List<String> allValues = new ArrayList<>();
        List<String> allKeys = new ArrayList<>();
        for (String keys: data.keySet()) {
            allKeys.add(keys);
        }
        for (String values: data.values()){
            allValues.add(values);
        }

        JSONObject jsonObject = new JSONObject();

        jsonObject.put(allKeys.get(0),allValues.get(0));
        jsonObject.put(allKeys.get(1),allValues.get(1));
        jsonObject.put(allKeys.get(2),allValues.get(2));
        jsonObject.put(allKeys.get(3),allValues.get(3));
        jsonObject.put(allKeys.get(4),allValues.get(4));
        jsonObject.put(allKeys.get(5),allValues.get(5));

        JSONArray jsonArray = new JSONArray();
        jsonArray.add(jsonObject);
        try{
            FileWriter file = new FileWriter("src/main/resources/Testing.json");
            file.write(jsonArray.toJSONString());
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Inserted into the Testing.json: "+jsonObject);
    }

    public void storingJsonIntoAClassObject() throws JsonProcessingException {
        JSONArray jsonArray = getJsonArray("src/main/resources/Testing.json");
        JSONObject jsonObject = (JSONObject) jsonArray.get(0);

        ObjectMapper objectMapper = new ObjectMapper();
        Employee employee = objectMapper.readValue(jsonObject.toJSONString(),Employee.class);

        System.out.println(employee.getName());
        System.out.println(employee.getCity());
        System.out.println(employee.getPermanent());
        System.out.println(employee.getPhone_number());
        System.out.println(employee.getRole());
        System.out.println(employee.getId());
    }
}

package com.methods;

import org.testng.annotations.DataProvider;

import java.util.Random;

public class DataProviderClass {

    @DataProvider
    public Object [][] dataForPhoneNumber (){
        Random random =new Random();
        return new Object[][]{{random.nextInt(999)},{random.nextInt(999)}};
    }
}

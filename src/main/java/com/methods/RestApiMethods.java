package com.methods;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import static com.methods.DataBaseMethods.getMapFromDBOfSingleRecord;

public class RestApiMethods {

    public static Map<String, String> parsingJsonToMap(String jsonR) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, String> jsonMap = objectMapper.readValue(jsonR, Map.class);
        return jsonMap;
    }

    public Map<String, String> getSpecificEmployee(String endpoint) throws IOException, SQLException {
        RestAssured.baseURI = "http://localhost:8090/";
        Response response2 = RestAssured
                .given()
                .when()
                .get(endpoint)
                .then()
                .log()
                .all()
                .extract().response();

        Map<String, String> map = (parsingJsonToMap(response2.asString()));
        return map;
    }

    public void testDbMatchesApi(String endpoint, String query) throws SQLException, IOException {
        Map<String, String> map3 = getSpecificEmployee(endpoint);
        System.out.println("Response Json Map : " + map3);

        Map<String, String> map2 = getMapFromDBOfSingleRecord(query);
        System.out.println("Database Query Map : " + map2);

        Assert.assertTrue(map3.get("name").equals(map2.get("name")));
    }

    public void createAnEmployee() throws IOException, SQLException {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/add";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "Mizanur");
        jsonObject.put("permanent", 1);
        jsonObject.put("phone_number", 347);
        jsonObject.put("role", "jqa");
        jsonObject.put("city", "Queens");

        JSONArray jsonArray = new JSONArray();
        jsonArray.add(jsonObject);

        Response response4 = RestAssured
                .given()
                .header("Content-Type", "application/json")
                .when()
                .body(jsonArray.toJSONString())
                .post(endpoint)
                .then()
                .log()
                .all()
                .extract().response();

        testDbMatchesApi("/employees/15", "SELECT name FROM worldOfAutomation.employee WHERE id=15");
    }

    public void deleteAnEmployee() throws IOException, SQLException {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/delete/10";
        Response response3 = RestAssured
                .given()
                .when()
                .delete(endpoint)
                .then()
                .log()
                .all()
                .extract().response();

        Map<String, String> map2 = getMapFromDBOfSingleRecord("SELECT name FROM worldOfAutomation.employee WHERE id=10");
        System.out.println("Database Query Map : " + map2);

        Assert.assertTrue(map2.isEmpty());
    }

    public void updateAnEmployee() throws IOException, SQLException {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/update/11";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "fahim");
        jsonObject.put("permanent", "2");
        jsonObject.put("phone_number", 345);
        jsonObject.put("role", "qa");
        jsonObject.put("city", "Bronx");

        Response response5 = RestAssured
                .given()
                .header("Content-Type", "application/json")
                .when()
                .body(jsonObject.toJSONString())
                .put(endpoint)
                .then()
                .log()
                .all()
                .extract().response();

        testDbMatchesApi("/employees/11", "SELECT name FROM worldOfAutomation.employee WHERE id=11");
    }

    public void getAllEmployees() {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/all";
        Response response1 = RestAssured
                .given()
                .when()
                .get(endpoint)
                .then()
                .log()
                .all()
                .extract().response();
    }

    public void createMultipleEmployees(Object data) throws IOException, SQLException {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/add";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "Dey");
        jsonObject.put("permanent", 2);
        jsonObject.put("phone_number", data);
        jsonObject.put("role", "qa");
        jsonObject.put("city", "New York");

        JSONArray jsonArray = new JSONArray();
        jsonArray.add(jsonObject);

        Response response4 = RestAssured
                .given()
                .header("Content-Type", "application/json")
                .when()
                .body(jsonArray.toJSONString())
                .post(endpoint)
                .then()
                .log()
                .all()
                .extract().response();
    }
}

package com.automation.tests;

import com.base.TestBase;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.methods.DataBaseMethods;
import com.methods.DataProviderClass;
import com.methods.JsonFileProcessing;
import com.methods.RestApiMethods;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;

public class Tests extends TestBase {

    DataBaseMethods dataBaseMethods = new DataBaseMethods();
    RestApiMethods restApiMethods = new RestApiMethods();
    JsonFileProcessing jsonFileProcessing = new JsonFileProcessing();

    @Test(enabled = false)
    public void testRunDB() throws SQLException, IOException {
        dataBaseMethods.validateTestRunForEmployees();
    }

    @Test(enabled = false)
    public void testRunApi() throws SQLException, IOException {
        restApiMethods.getSpecificEmployee("/employees/1");
    }

    @Test(enabled = false)
    public void testDbAndApiTogether() throws SQLException, IOException {
        restApiMethods.testDbMatchesApi("/employees/2", "SELECT name FROM worldOfAutomation.employee WHERE id=2");
    }

    @Test(enabled = false)
    public void testCreatingAnEmployee() throws SQLException, IOException {
        restApiMethods.createAnEmployee();
    }

    @Test(enabled = false)
    public void testDeletingAnEmployee() throws SQLException, IOException {
        restApiMethods.deleteAnEmployee();
    }

    @Test(enabled = false)
    public void testUpdatingAnEmployee () throws SQLException, IOException {
        restApiMethods.updateAnEmployee();
    }

    @Test (dataProvider = "dataForPhoneNumber", dataProviderClass = DataProviderClass.class, enabled = false)
    public void testCreatingMultipleEmployees (Object data) throws SQLException, IOException {
        restApiMethods.createMultipleEmployees(data);
    }

    @Test (enabled = false)
    public void testRetrievalAndStoring () throws SQLException, IOException {
        jsonFileProcessing.getRecordsFromDB();
    }

    @Test (enabled = false)
    public void testStoringJsonIntoClassObject () throws JsonProcessingException {
        jsonFileProcessing.storingJsonIntoAClassObject();
    }
}
